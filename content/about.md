{
  "title": "About",
  "description": "A little bit about the club",
  "date": "2017-02-28",
  "url": "about/",
  "type": "about",
  "tags": [],
  "aliases":["profile/"]
}

This page is lacking content.  If it did have some content,
you could expect to learn a little about this brewing club.
For instance a brief bit about when and how the club was
started, and why it exists.

We could also include info on how to contact us, what events
we frequent, and who knows what else.

+++
comments = true
description = ""
image = "/img/about-bg.jpg"
categories = ["Events"]
tags = ["events"]
date = "2017-02-28T20:36:58-08:00"
title = "Upcoming Events"

+++

We're going to be at a few events in the next few months,
hopefully you can join us!

- <a href="http://www.sistershomebrewfestival.com/" target="_blank">Sisters Homebrew Festival</a> - several
club members will bringing brews to the event.
- Some other event
- Something else we are doing

By the way, this page could also be a regular page, for instance
next to "About" in the header.

+++
date = "2017-02-28T15:32:49-08:00"
title = "New Site"

+++

Welcome to the new home of the Strange Brew Homebrew Club!

There's not a whole lot of content here just yet but we will
be adding more in the near future - stay tuned!
